#include "Bishop.h"



Bishop::Bishop(PieceName type, char color) :ChessPiece(type, color)
{
	this->_color = color;
	this->_type = type;
}


Bishop::~Bishop()
{
}

bool Bishop::isValidMove(Location& src, Location& dest, ChessPiece* b[WIDTH][LENGTH]) const
{
	Location start(dest);
	Location end(src);
	int adder = 1;

	//checking if the piece moving diagnolly
	if (abs(dest.getX() - src.getX()) == abs(dest.getY() - src.getY())) {
		//cheking if diagnol is up and to the left
		if (src.getX() > dest.getX() && dest.getY() > src.getY()) {
			for (int i = 1; (src.getY() + i) != dest.getY(); i++) {
				//checking that the next Cube is empty
				if (b[src.getY() + i][src.getX() - adder] &&
					b[src.getY() + i][src.getX() - adder] != b[src.getY()][src.getX()]) {
					return false; // the move is illegeal
				}
				adder++;
			}
		}
		//checking if the diagnol moving down to the right
		else if (dest > src) {
			for (int i = 1; (src.getX() + i) != dest.getX(); i++) {
				//checking that the next Cube is empty
				if (b[src.getY() + i][src.getX() + i] &&
					b[src.getY() + i][src.getX() + i] != b[src.getY()][src.getX()]) {
					return false; // the move is illegeal
				}
			}
		}
		//checking if the diagnol moving down to the left
		else if (src > dest) {
			for (int i = 1; (start.getX() + i) != end.getX(); i++) {
				//checking that the next Cube is empty
				if (b[start.getY() + i][start.getX() + i] &&
					b[start.getY() + i][start.getX() + i] != b[src.getY()][src.getX()]) {
					return false; // the move is illegeal
				}
			}
		}
		//the diagnol moving up to the right
		else {
			for (int i = 1; (src.getX() + i) != dest.getX(); i++) {
				//checking that the next Cube is empty
				if (b[src.getY() - adder][src.getX() + i] &&
					b[src.getY() - adder][src.getX() + i] != b[src.getY()][src.getX()]) {
					return false; // the move is illegeal
				}
				adder++;
			}
		}
	}
	else {
		return false; // the move is illegeal
	}
	return true; // the move is legeal
}
