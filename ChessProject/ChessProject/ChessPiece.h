#pragma once
#include <iostream>
#include "Location.h"
#define WIDTH 8
#define LENGTH 8
using std::string;

enum PieceName
{
	RookName, BishopName, QueenName, KingName, KnightName, PawnName
};


class ChessPiece
{

public:
	ChessPiece(PieceName type, char color) : color(color), type(type) {}
	~ChessPiece() {}
	ChessPiece& operator=(ChessPiece& other);
	virtual bool isValidMove(Location& src, Location& dest, ChessPiece* b[WIDTH][LENGTH]) const = 0;
	virtual PieceName getType() const = 0;
	virtual char getColor() const = 0;


protected:
	char color;
	PieceName type;
};

