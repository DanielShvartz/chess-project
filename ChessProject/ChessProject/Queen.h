#pragma once
#include "ChessPiece.h"
#include "Rook.h"
#include "Bishop.h"

class Queen :public Rook, public Bishop
{
public:
	Queen(PieceName type, char color);
	~Queen();
	virtual bool isValidMove(Location& src, Location& dest, ChessPiece* b[WIDTH][LENGTH]) const;
	virtual PieceName getType() const { return this->_type; }
	virtual char getColor() const { return this->_color; }

private:
	PieceName _type;
	char _color;
};

