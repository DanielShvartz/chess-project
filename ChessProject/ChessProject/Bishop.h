#pragma once
#include "ChessPiece.h"
class Bishop :virtual public ChessPiece
{
public:
	Bishop(PieceName type, char color);
	~Bishop();
	virtual bool isValidMove(Location& src, Location& dest, ChessPiece* b[WIDTH][LENGTH]) const;
	virtual PieceName getType() const { return this->_type; }
	virtual char getColor() const { return this->_color; }

private:
	PieceName _type;
	char _color;
};
