#include "Pawn.h"

bool Pawn::isValidMove(Location& src, Location& dest, ChessPiece* b[WIDTH][LENGTH]) const
{
	//checking if the pawn trying to move forward to empty Cube
	if (dest.getX() == src.getX() && !b[dest.getY()][dest.getX()])
	{
		// checing if the black pawn tried to move 2 steps in the beginning
		if ((dest.getY() - src.getY() == 1 || (dest.getY() - src.getY() == 2 && src.getY() == 1))
			&& b[src.getY()][src.getX()]->getColor() == 'b')
			return true;
		// checing if the black pawn tried to move 2 steps in the beginning
		else if ((src.getY() - dest.getY() == 1 || (src.getY() - dest.getY() == 2 && src.getY() == 6))
			&& b[src.getY()][src.getX()]->getColor() == 'w')
			return true;
	}
	else if (b[dest.getY()][dest.getX()] != nullptr)// checking if the pawn is trying to eat
	{
		//white
		if (abs(dest.getX() - src.getX()) == 1 && dest.getY() - src.getY() == -1 &&
			b[src.getY()][src.getX()]->getColor() == 'w')
			return true;
		//black
		else if (abs(dest.getX() - src.getX()) == 1 && dest.getY() - src.getY() == 1 &&
			b[src.getY()][src.getX()]->getColor() == 'b')
			return true;
	}
	return false;
}
