#pragma once
class Location
{
private:
	int _x; // my dream was always to make a location class, so i made one
	int _y; // the location will have me using indexes
public:
	Location(int x, int y); // get an index
	Location(const Location& otherLocation) { *this = otherLocation; } // we get another location so we just use a copy constrantor
	~Location(); 
	int getX() const; //gettes
	int getY() const;
	bool operator==(Location& otherLocation) const; // to check if location is the same location
	bool operator>(Location& otherLocation) const { return ((this->getX() > otherLocation.getX()) && (this->getY() > otherLocation.getY())); } // to check if a location is bigger then a given location
	Location& operator=(const Location& otherLocation);
	void setX(int x) { this->_x = x; } //setters
	void setY(int y) { this->_y = y; }
};

