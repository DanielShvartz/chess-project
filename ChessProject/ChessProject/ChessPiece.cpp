#include <iostream>
#include "ChessPiece.h"
#include "GameManager.h"

ChessPiece& ChessPiece::operator=(ChessPiece& other)
{
	this->color = other.getColor();
	this->type = other.getType();
	return *this;
}
