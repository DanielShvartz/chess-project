#pragma once

#include "ChessPiece.h"
#include "Location.h"

#define STEP 1

class King : public ChessPiece
{
public:

	King(PieceName type, char color);
	~King();
	virtual bool isValidMove(Location& src, Location& dest, ChessPiece* b[WIDTH][LENGTH]) const;
	virtual PieceName getType() const;
	virtual char getColor() const;

private:
	PieceName _type;
	char _color;
};
