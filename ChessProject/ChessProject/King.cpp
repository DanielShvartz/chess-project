#include "King.h"
King::~King()
{

}
King::King(PieceName type, char color) : ChessPiece(type, color) {
	this->_type = type;
	this->_color = color;
}
bool King::isValidMove(Location& src, Location& dest, ChessPiece* b[WIDTH][LENGTH]) const {
	//making sure that the king wont move more then 1 step
	if (abs(src.getX() - dest.getX()) <= STEP && abs(src.getY() - dest.getY()) <= STEP) {
		return true;
	}
	return false; // the move is illegeal
}
PieceName King::getType() const
{ return this->_type; }

char King::getColor() const
{ return this->_color; }