#include "GameManager.h"


enum type {
	VALID_MOVE,
	CHESS_BEEN_MADE,
	NOT_YOUR_TURN,
	DST_NOT_FREE,
	CHESS_WILL_ACCURE,
	OUT_IF_BOUNDS,
	ILLEGEAL_MOVEMENT,
	STAYING_SAME_CUBE,
	CHECK_MATE
};


/*
the c'tor of the class board
this c'tor recieves the string that holds the position of each tool
and puts the correct piece in the correct index in the matrix
*/

GameManager::GameManager(string& data, char player) :_WhiteKing(Location(0, 0)), _BlackKing(Location(0, 0)) {
	this->player = player; // setting the starting player
	this->_checkingMate = false; //setting this field, so the check function wont run
	int index = 0;
	for (int i = 0; i < LENGTH; ++i)
	{
		for (int j = 0; j < WIDTH; ++j)
		{
			switch (data[index]) // running on all the tools in the string
			{
			case 'r': // creating new black rook
				this->board[i][j] = new Rook(RookName, 'b');
				this->tempBoard[i][j] = new Rook(RookName, 'b'); // saving the rook in the temp board
				break;
			case 'R': // creating new white rook
				this->board[i][j] = new Rook(RookName, 'w');
				this->tempBoard[i][j] = new Rook(RookName, 'w');// saving the rook in the temp baord
				break;
			case 'b': // creating new black bishop 
				this->board[i][j] = new Bishop(BishopName, 'b');
				this->tempBoard[i][j] = new Bishop(BishopName, 'b');
				break;
			case 'B':// creating new white bishop
				this->board[i][j] = new Bishop(BishopName, 'w');
				this->tempBoard[i][j] = new Bishop(BishopName, 'w');
				break;
			case 'q': // creating new black queen
				this->board[i][j] = new Queen(QueenName, 'b');
				this->tempBoard[i][j] = new Queen(QueenName, 'b');
				break;
			case 'Q': // creating new white queen
				this->board[i][j] = new Queen(QueenName, 'w');
				this->tempBoard[i][j] = new Queen(QueenName, 'w');
				break;
			case 'n': // creating new black knight
				this->board[i][j] = new Knight(KnightName, 'b');
				this->tempBoard[i][j] = new Knight(QueenName, 'b');
				break;
			case 'N': // creating new white knight
				this->board[i][j] = new Knight(KnightName, 'w');
				this->tempBoard[i][j] = new Knight(KnightName, 'w');
				break;
			case 'p': // creating new black pawn
				this->board[i][j] = new Pawn(PawnName, 'b');
				this->tempBoard[i][j] = new Pawn(PawnName, 'b');
				break;
			case 'P': // creating new white queen
				this->board[i][j] = new Pawn(PawnName, 'w');
				this->tempBoard[i][j] = new Pawn(PawnName, 'w');
				break;
			case 'k': // creating new black king
				this->board[i][j] = new King(KingName, 'b');
				this->tempBoard[i][j] = new King(KingName, 'b');
				this->_BlackKing = Location(j, i);
				break;
			case 'K': // creating new white king
				this->board[i][j] = new King(KingName, 'w');
				this->tempBoard[i][j] = new King(KingName, 'w');
				this->_WhiteKing = Location(j, i);
				break;
			case '#': // creating empty location by putting nullptr
				this->board[i][j] = nullptr;
				this->tempBoard[i][j] = nullptr;
				break;
			default:
				break;
			}
			index++;
		}
	}
}


GameManager::~GameManager()
{
	for (int i = 0; i < LENGTH; i++)
	{
		for (int j = 0; j < WIDTH; j++)
		{
			if (this->board[i][j] != nullptr)
			{
				this->board[i][j]->~ChessPiece(); //D'tor of the object
			}
			if (this->tempBoard[i][j] != nullptr)
			{
				this->tempBoard[i][j]->~ChessPiece();
			}
		}
	}
}

char GameManager::isValidMove(string& data)
{
	Location src = getLocation(data.substr(0, 2)); // getting the src Cube from the string
	Location dest = getLocation(data.substr(2, 2)); // getting the dest Cube from the string
	Location* temp[2] = { &this->_BlackKing, &this->_WhiteKing };
	//making sure that the player is trying to move his piece
	if (this->board[src.getY()][src.getX()] == nullptr ||
		this->board[src.getY()][src.getX()]->getColor() != this->player)
	{
		return NOT_YOUR_TURN + 48;
	}
	//making sure that the player is not trying to eat his own pieces
	else if (this->board[dest.getY()][dest.getX()] != nullptr &&
		this->player == this->board[dest.getY()][dest.getX()]->getColor())
	{
		return DST_NOT_FREE + 48;
	}
	// making sure that the cubes are valid
	else if (src.getX() >= LENGTH || src.getX() < 0 || src.getY() >= WIDTH || src.getY() < 0 ||
		dest.getX() >= LENGTH || dest.getX() < 0 || dest.getY() >= WIDTH || dest.getY() < 0)
	{
		return OUT_IF_BOUNDS + 48;
	}
	//making sure that the piece can reach the dest cube
	else if (!this->board[src.getY()][src.getX()]->isValidMove(src, dest, this->board))
	{
		return ILLEGEAL_MOVEMENT + 48;
	}
	// making sure that the player is not trying to stay in the same cube
	else if (src == dest) {
		return STAYING_SAME_CUBE + 48;
	}
	else // if the move passed all the conditions, the move is valid
	{
		this->updateBoard(src, dest); // updating the board
		updateKings(dest); // updating the kings
		if (this->checkChess(_WhiteKing, 'b')) { // cheking the white king
			if (this->player == 'b')//black has made a check on the white 
			{
				if (!this->_checkingMate) { //checking if the program checks for mate
					if (this->checkMate(this->_WhiteKing, 'w')) { // checking for mate
						return CHECK_MATE + 48; // if there is a mate
					}
					this->restoreBoard(); // if there isnt a mate, the board is restored
										  //setPlayer(this->player); // setting the next player
				}
				return CHESS_BEEN_MADE + 48;
			}
			else//white has chess and doesn't cancel it
			{
				updateKings(*temp[1]);
				restoreBoard(); // restoring the board
				return CHESS_WILL_ACCURE + 48;
			}
		}
		else if (this->checkChess(_BlackKing, 'w')) { // checking the black king
			if (this->player == 'w')//white has made a check on the black
			{
				if (!this->_checkingMate) {
					if (this->checkMate(this->_BlackKing, 'b')) {
						return CHECK_MATE + 48;
					}
					this->restoreBoard();
				}
				return CHESS_BEEN_MADE + 48;
			}
			else //black has chess and doesn't cancel it
			{
				updateKings(*temp[0]);
				restoreBoard();
				return CHESS_WILL_ACCURE + 48;
			}
		}
		if (!this->_checkingMate) {
			setPlayer(this->player);
		}
		return VALID_MOVE + 48;
	}
}

/*
this function is over loading the isValidMove function
this function receives the src and the dest cubes,
turns them into a string ("exemple: e2e4") and calls to the original isValidMove function
*/

char GameManager::isValidMove(Location& src, Location& dest) {
	string data = "    ";
	data[1] = 56 - src.getY();
	data[0] = src.getX() + 97;
	data[3] = 56 - dest.getY();
	data[2] = dest.getX() + 97;
	if (data[3] > '8' || data[3] < '0') { // checking the bounds
		return '7';
	}
	return this->isValidMove(data); // calling the function
}

/*
this function updates the kings location
*/
void GameManager::updateKings(Location& dest)
{
	// making sure that the king was moved
	if (this->board[dest.getY()][dest.getX()] &&
		this->board[dest.getY()][dest.getX()]->getType() == KingName) {
		//making sure that the white king is moved
		if (this->board[dest.getY()][dest.getX()]->getColor() == 'w') {
			this->_WhiteKing = dest;
		}
		// making sure that the black king is moved
		else {
			this->_BlackKing = dest;
		}
	}
}

/*
this function gets a string (e2 - a cube) and returns a Cube
*/

Location GameManager::getLocation(string data) const
{
	int y = abs(data[1] - 56);//casting
	int x = abs(data[0] - 97);//casting
	Location c = Location(x, y);
	return c;
}


/*
-the function being called if and only if
the move is valid
-The function updates the board
input: Cube src and Cube dest
output: none
*/
void GameManager::updateBoard(Location& src, Location& dest)
{
	//saving the board
	for (int i = 0; i < LENGTH; i++)
	{
		for (int j = 0; j < WIDTH; j++)
		{
			tempBoard[i][j] = board[i][j];
		}
	}
	this->board[dest.getY()][dest.getX()] = this->board[src.getY()][src.getX()];//putting the src piece in the dest
	this->board[src.getY()][src.getX()] = nullptr;//make the src empty
}

/*
printing the board
*/
void GameManager::printBoard()
{
	for (int i = 0; i < WIDTH; i++)
	{
		for (int j = 0; j < LENGTH; j++)
		{
			if (this->board[i][j] == nullptr)
			{
				cout << "# ";
			}
			else
			{
				switch (this->board[i][j]->getType())
				{
				case RookName:
					if (this->board[i][j]->getColor() == 'b')
					{
						cout << "r ";
					}
					else
					{
						cout << "R ";
					}
					break;
				case BishopName:
					if (this->board[i][j]->getColor() == 'b')
					{
						cout << "b ";
					}
					else
					{
						cout << "B ";
					}
					break;
				case QueenName:
					if (this->board[i][j]->getColor() == 'b')
					{
						cout << "q ";
					}
					else
					{
						cout << "Q ";
					}
					break;
				case KingName:
					if (this->board[i][j]->getColor() == 'b')
					{
						cout << "k ";
					}
					else
					{
						cout << "K ";
					}
					break;
				case KnightName:
					if (this->board[i][j]->getColor() == 'b')
					{
						cout << "n ";
					}
					else
					{
						cout << "N ";
					}
					break;
				case PawnName:
					if (this->board[i][j]->getColor() == 'b')
					{
						cout << "p ";
					}
					else
					{
						cout << "P ";
					}
					break;
				default:
					cout << "invalid note!";
					break;
				}
			}
		}
		cout << endl;
	}
}


/*
Arrives to the function if the move is valid
Function switches the player
input: the current color
output: none
*/
void GameManager::setPlayer(char color)
{
	if (color == 'b')
	{
		this->player = 'w';
	}
	else
	{
		this->player = 'b';
	}
}


/*
this function checks if there is a threat on the given king
input: the location of the given king and the color of the the pieces that can
make a threat on the king
*/

bool GameManager::checkChess(Location& kingLocation, char color)
{
	Location temp(0, 0);
	for (int i = 0; i < LENGTH; i++)
	{
		for (int j = 0; j < WIDTH; j++)
		{
			if (this->board[i][j] != nullptr && this->board[i][j]->getColor() == color)
			{
				//temp is the piece that the program checking if he can reach to king
				temp.setX(j); temp.setY(i);
				if (this->board[i][j]->isValidMove(temp, kingLocation, this->board))
				{
					return true; // the threat exsits
				}
			}
		}
	}
	return false; // the king is safe
}

/*
this function resotres the board
*/

void GameManager::restoreBoard() {
	for (int i = 0; i < LENGTH; i++) {
		for (int j = 0; j < WIDTH; j++)
		{
			// checking if the kings are moved, if the do, saving there location
			if (this->tempBoard[i][j]) {
				if (this->tempBoard[i][j]->getType() == KingName && this->tempBoard[i][j]->getColor() == 'w') {
					this->_WhiteKing.setX(j);
					this->_WhiteKing.setY(i);
				}
				else if (this->tempBoard[i][j]->getType() == KingName && this->tempBoard[i][j]->getColor() == 'b') {
					this->_BlackKing.setX(j);
					this->_BlackKing.setY(i);
				}
			}
			this->board[i][j] = this->tempBoard[i][j];
		}
	}
}

/*
this fucntion checks if the king is dead ( mate )
first, the function will check if the king can escape the chess
and if not, the function will call other funcion that will check is any other piece can block
the chess.
input: the location of the given king
*/

bool GameManager::checkMate(Location& kingLocation, char color) {
	this->_checkingMate = true; // for later, to prevent infinte loop
	this->player = color;
	Location listOfLocation[] = { Location(-1,1),Location(-1,0),Location(-1,-1),Location(0,-1),Location(0,1),Location(1,-1),
		Location(1,0),Location(1,1) };
	Location temp(0, 0);
	//running on all the possible cubes the king can escape to 
	for (int i = 0; i < 8; i++) {
		temp.setY(kingLocation.getY() + listOfLocation[i].getY());
		temp.setX(kingLocation.getX() + listOfLocation[i].getX());
		// temp will contain the location possible Cube that will save him from the check
		if (this->isValidMove(kingLocation, temp) == '0') {
			this->_checkingMate = false;
			return false; // the king can move to other location and hide from the threat
		}
	}
	return this->canBlock(kingLocation, color);// checking if any piece can block the threat
}

/*
this function checks is any other piece can prevent the mate
by blocking the chess or eating the piece
*/

bool GameManager::canBlock(Location& kingLocation, char color) {
	/*
	in general, this function runs on all the pieces the player has and moves the
	piece to every cube in the board, and checks if the current move prevented the mate
	*/
	Location src(0, 0);
	Location dest(0, 0);
	for (int i = 0; i < 8; i++) {
		for (int j = 0; j < 8; j++) {
			for (int a = 0; a < 8; a++) {
				for (int b = 0; b < 8; b++) {
					//checking if the piece is belong to the player
					if (this->board[i][j] &&
						this->board[i][j]->getColor() == color) {
						src.setX(j); src.setY(i); dest.setX(b); dest.setY(a);
						//checking if the current piece in the current location will
						// prevent the chess
						if (this->isValidMove(src, dest) == '0') {
							this->_checkingMate = false; // canceling the checking mate field
														 // so now the checkMate function will activate in the situation of chess
							return false; // there is a piece that can block the threat
						}
					}
				}
			}
		}
	}
	this->_checkingMate = false;
	return true; // there is mate :(
}
