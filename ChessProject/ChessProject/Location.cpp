#include "Location.h"
Location::Location(int x, int y)
{
	_x = x;
	_y = y;
}

Location::~Location()
{
}

int Location::getX() const
{
	return _x;
}

int Location::getY() const
{
	return _y;
}
bool Location::operator==(Location& otherLocation) const
{
	return (_x == otherLocation.getX() && _y == otherLocation.getY());
}
Location& Location::operator=(const Location& otherLocation)
{
	_x = otherLocation.getX();
	_y = otherLocation.getY();
	return *this;
}

