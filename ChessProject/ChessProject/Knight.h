#pragma once
#include "ChessPiece.h"
class Knight : public ChessPiece
{
public:
	Knight(PieceName type, char color);
	virtual bool isValidMove(Location& src, Location& dest, ChessPiece* b[WIDTH][LENGTH]) const;
	virtual PieceName getType() const;
	virtual char getColor() const;

private:
	PieceName _type;
	char _color;
};
