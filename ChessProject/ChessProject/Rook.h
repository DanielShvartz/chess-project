#pragma once
#include "ChessPiece.h"

class Rook :virtual public ChessPiece
{
public:
	Rook(PieceName type, char color);
	~Rook();
	virtual bool isValidMove(Location& src, Location& dest, ChessPiece* b[WIDTH][LENGTH]) const;
	virtual PieceName getType() const { return this->_type; }
	virtual char getColor() const { return this->_color; }

private:
	char _color;
	PieceName _type;
};

