#pragma once
#include <iostream>
#include <stack>
#include "ChessPiece.h"
#include "Location.h"
#include "Bishop.h"
#include "Rook.h"
#include "Queen.h"
#include "Knight.h"
#include "Pawn.h"
#include "King.h"

#define LENGTH 8
#define WIDTH 8

using std::cout;
using std::endl;


class GameManager
{
public:
	GameManager(string& data, char player);
	~GameManager();
	char isValidMove(string& data);
	void printBoard();
	void updateBoard(Location& src, Location& dest);



private:
	Location _WhiteKing;
	Location _BlackKing;
	ChessPiece* board[LENGTH][WIDTH];
	ChessPiece* tempBoard[LENGTH][WIDTH];
	bool _checkingMate;

	char player; // b = black, w = white
	Location getLocation(string data) const;

	void updateKings(Location& dest);
	void setPlayer(char color);
	void restoreBoard();

	char isValidMove(Location& src, Location& dest);
	bool checkChess(Location& kingLocation, char color);
	bool checkMate(Location& kingLocation, char color);
	bool canBlock(Location& kingLocation, char color);
};
