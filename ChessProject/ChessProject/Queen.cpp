#include "Queen.h"


Queen::Queen(PieceName type, char color) :ChessPiece(type, color), Rook(type, color), Bishop(type, color)
{
	this->_type = type;
	this->_color = color;
}



Queen::~Queen()
{
}

bool Queen::isValidMove(Location& src, Location& dest, ChessPiece* b[WIDTH][LENGTH]) const
{
	//queen is a combination of the rook and the bishop, so if the queen is trying to move to
	// cube that the bishop/rook cant move to there, the move is invallid
	if (Rook::isValidMove(src, dest, b) || Bishop::isValidMove(src, dest, b))
		return true;

	else
		return false;
}
