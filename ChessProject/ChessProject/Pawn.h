#pragma once
#include "ChessPiece.h"
class Pawn :public ChessPiece
{
public:
	Pawn(PieceName type, char color) :ChessPiece(type, color) {
		this->_type = type;
		this->_color = color;
	}
	virtual bool isValidMove(Location& src, Location& dest, ChessPiece* b[WIDTH][LENGTH]) const;
	virtual PieceName getType() const { return this->_type; }
	virtual char getColor() const { return this->_color; }

private:
	PieceName _type;
	char _color;
};
