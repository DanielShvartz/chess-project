#include "Rook.h"



Rook::Rook(PieceName type, char color) :ChessPiece(type, color)
{
	this->_color = color;
	this->_type = type;
}


Rook::~Rook()
{

}


bool Rook::isValidMove(Location& src, Location& dest, ChessPiece* b[WIDTH][LENGTH]) const
{
	Location start(dest);
	Location end(src);

	if (dest.getX() > src.getX() || dest.getY() > src.getY()) {
		start = src;
		end = dest;
	}
	//checking if the rook is moving horizontolly
	if (src.getX() == dest.getX())
	{
		//running on all the Cubes horizontolly from the begenining to the end
		for (int i = 1; start.getY() + i < end.getY(); i++) 
			//checking that the rook can step in the current Cube
			if (b[start.getY() + i][start.getX()] &&
				b[start.getY() + i][start.getX()] != b[src.getY()][src.getX()]) 
				return false;
	}
	//checking if the rook is trying to move vertically
	else if (src.getY() == dest.getY())
	{
		//running on all the Cubes vertically from the begenining to the end
		for (int i = 1; (start.getX() + i) < end.getX(); i++)
			//checking that the rook can step in the current Cube
			if (b[start.getY()][start.getX() + i] &&
				b[start.getY()][start.getX() + i] != b[src.getY()][src.getX()])
				return false;
	}
	else
	{
		return false;
	}
	return true;//if arrives to here ,everything is valid
}

