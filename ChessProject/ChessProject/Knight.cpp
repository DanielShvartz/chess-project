#include "Knight.h"
#define KNIGHT_FIRST_MOVE 2
#define KNIGHT_SECOND_MOVE 1

Knight::Knight(PieceName type, char color) : ChessPiece(type, color)
{
	_type = type;
	_color = color;
}

bool Knight::isValidMove(Location& src, Location& dest, ChessPiece* b[WIDTH][LENGTH]) const
{
	if (abs(dest.getY() - src.getY()) == KNIGHT_FIRST_MOVE)
	{
		if (abs(dest.getX() - src.getX()) != KNIGHT_SECOND_MOVE)
		{
			return false;
		}
	}
	else if (abs(dest.getX() - src.getX()) == KNIGHT_FIRST_MOVE)
	{
		if (abs(dest.getY() - src.getY()) != KNIGHT_SECOND_MOVE)
		{
			return false;
		}
	}
	else
	{
		return false;
	}
	return true;
}

PieceName Knight::getType() const
{
	return this->_type;
}
char Knight::getColor() const
{
	return this->_color;
}
