#include "Pipe.h"
#include "GameManager.h"
#include <iostream>
#include <thread>

using namespace std;
int main()
{
	srand(time_t(NULL));
	Pipe p;
	bool isConnect = p.connect();

	string ans;
	while (!isConnect)
	{
		cout << "cant connect to graphics" << endl;
		cout << "Do you try to connect again or exit? (0-try again, 1-exit)" << endl;
		cin >> ans;

		if (ans == "0")
		{
			cout << "trying connect again.." << endl;
			Sleep(5000);
			isConnect = p.connect();
		}
		else
		{
			p.close();
			return 0;
		}
	}


	char msgToGraphics[1024];
	// msgToGraphics should contain the board string accord the protocol
	// YOUR CODE
	strcpy_s(msgToGraphics, "rnbkqbnrpppppppp################################PPPPPPPPRNBKQBNR0"); // just example...


	p.sendMessageToGraphics(msgToGraphics);   // send the board string
	string toBoard = "rnbkqbnrpppppppp################################PPPPPPPPRNBKQBNR";
	GameManager b(toBoard, 'w'); // initiating the board, with the white player to start
	b.printBoard(); // printing the board in the beginning
					// get message from graphics
	string msgFromGraphics = p.getMessageFromGraphics();
	msgToGraphics[1] = '\0'; // set the string
	while (msgFromGraphics != "quit")
	{
		msgToGraphics[0] = b.isValidMove(msgFromGraphics); // get the char from the isValidMove func
		b.printBoard(); // print the board
						// return result to graphics		
		p.sendMessageToGraphics(msgToGraphics);   //send the msg to the graphics

												  // get message from graphics
		msgFromGraphics = p.getMessageFromGraphics();
	}
	p.close();
}
